class UsersController < ApplicationController
  before_action :set_user, only: [:show, :destroy]
  
  def index
    @interviewers = User.where(admin: true)
    @interviewees = User.where(admin: false)
  end
  
  def new
    @user = User.new
  end
  
  def create
    @user = User.new(user_params)
    if @user.save
      redirect_to user_path(@user.id), notice: "新規ユーザー作成しました"
    else
      render 'new'
    end
  end
  
  def show
  end
  
  def destroy
    @user.destroy
  end
  
  private
  
  def user_params
    params.require(:user).permit(:name, :email, :password, :password_confirmation, :admin)
  end
  
  def set_user
    @user = User.find(params[:id])
  end
end
