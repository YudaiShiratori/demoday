Rails.application.routes.draw do
  get 'tops/index'

  resources :users
  resources :sessions, only: [:new, :create, :destroy]
  resources :schedules, only: [:new, :create, :destroy, :show]
  resources :tops, only: [:index]
end
